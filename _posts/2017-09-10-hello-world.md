---
layout: post
title: "Hello world!"
author: "Oswaldo"
---

Hey!

After using [Medium](https://medium.com/@olsfer) for a while, I came to the conclusion it's not the proper place to the content I'm intended to generate, so here we are!

The intention here is mainly sharing thoughts on software and my personal hobbies, which include open-source software, board games, digital games and music in general.

It's _not_ my intention to click-bait, or anything close to something like most blogs on Medium. It's actually the reason I _don't_ want to follow-up there in the first place.

So thanks for reading if you're not me, and enjoy the ride :)

_The end_
