---
layout: post
title: "Percentile measurements"
author: "Oswaldo"
---

Most of the time, _mean_ times are not sufficient as web-performance-wise measurements (e.g. request time). That explains
why most monitoring services present something called _percentile_ measurement.

Known on statistics studies, percentile is the percentage where values below a given value (or score) should be found. For example:

#### 95th percentile value of the request time on a given point in time is 250ms

This means 95% of the sample values being measured are below 250ms, and 5% above.

Here's a program written in golang which output values which represent the percentiles of a given dataset:

```golang
package main

import (
	"fmt"
)

func main() {
	dataSet := [10]int{2, 3, 5, 7, 11, 13, 15, 20, 34, 40}

	fmt.Println("10th percentile", percentileOf(10, dataSet))
	fmt.Println("80th percentile", percentileOf(80, dataSet))

	// 10th percentile [2 0 0 0 0 0 0 0 0 0]
	// 80th percentile [2 3 5 7 11 13 15 20 0 0]
}

func percentileOf(value int, dataSet [10]int) [10]int {
	percentileValues := [10]int{}
	positionOfPercentile :=
		int((float64(value) / float64(100)) * float64(len(dataSet)))

	for i := 0; i < positionOfPercentile; i += 1 {
		percentileValues[i] = dataSet[i]
	}

	return percentileValues
}
```

_The end_
